SHELL := /bin/sh

IMAGE ?= localhost/achivka2
VERSION ?= latest

PYINSTALLER_BASE := \
	--noconfirm \
	--clean \
	--onefile

PYINSTALLER_OPTIONS_SRV := \
	$(PYINSTALLER_BASE) \
	--name app \
	main.py

PYINSTALLER_OPTIONS_LOAD := \
	$(PYINSTALLER_BASE) \
	--name load-$(VERSION) \
	load.py

.PHONY: build push build_load

build:
	docker build --pull \
		--build-arg \
		PYINSTALLER_ARG="$(PYINSTALLER_OPTIONS_SRV)" \
		-t $(IMAGE):latest \
		-t $(IMAGE):$(VERSION) .

build_load:
	poetry run pyinstaller $(PYINSTALLER_OPTIONS_LOAD)

cleanup:
	-rm -rf *.spec dist build

push:
	docker push $(IMAGE):$(VERSION)
	docker push $(IMAGE):latest
