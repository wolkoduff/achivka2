# pylint: disable=missing-docstring

import os
import time
import argparse
from http import HTTPStatus
from sanic import Sanic
from sanic.log import logger
from sanic.request import Request
from sanic.response import json, raw
from sanic.exceptions import SanicException, abort
from aioredis import create_redis_pool, Redis

app = Sanic(__name__)  # pylint: disable=invalid-name


@app.route("/cleanup", methods=["POST"])
async def cleanup(request: Request):
    redis: Redis = request.app.redis

    request.app.loop.create_task(redis.delete("numbers"))

    return raw(body=b"", status=HTTPStatus.NO_CONTENT)


@app.route("/incr", methods=["POST"])
async def incr(request: Request):
    try:
        num = int((request.json or {}).get("number", -1))
    except ValueError:
        num = -1

    if num < 0:
        abort(HTTPStatus.BAD_REQUEST, "wrong input")

    redis: Redis = request.app.redis

    is_exists = await redis.sismember("numbers", num)

    if is_exists:
        abort(HTTPStatus.BAD_REQUEST, "number exists")

    pipe = redis.pipeline()
    pipe.sadd("numbers", num)
    pipe.sadd("numbers", num - 1)

    await pipe.execute()

    return json({"number": num + 1})


@app.exception(SanicException)
async def exc_handler(request: Request, exception: SanicException):
    err_body = {
        "input": request.json,
        "error": str(exception),
        "code": exception.status_code,
    }
    logger.error(err_body)

    return json(err_body, status=exception.status_code)


@app.listener("after_server_start")
async def create_redis(s_app: Sanic, _):
    try:
        s_app.redis = await create_redis_pool(app.config.REDIS_URL)
    except Exception as exc:  # pylint: disable=broad-except
        logger.error("Failed connect to redis")
        logger.error(str(exc))
        s_app.stop()


@app.listener("after_server_stop")
async def destroy_redis(s_app: Sanic, _):
    if hasattr(s_app, "redis"):
        s_app.redis.close()
        logger.debug("Wait close")
        await s_app.redis.wait_closed()
        logger.debug("closed")


def parse_args():
    parser = argparse.ArgumentParser(
        description="Incr service",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "-p", "--port", dest="port", type=int, help="Port", default=8000
    )

    parser.add_argument(
        "--prod",
        dest="prod",
        action="store_true",
        help="Production mode",
        default=False,
    )

    parser.add_argument(
        "-r",
        "--redis",
        dest="redis",
        type=str,
        help="Redis uri",
        default="redis://localhost",
    )

    return parser.parse_known_args()[0]


def run(run_config: argparse.Namespace):
    os.environ["TZ"] = "UTC"
    time.tzset()

    redis_url = os.getenv("REDIS_URL", run_config.redis)
    app.config.setdefault("REDIS_URL", redis_url)

    app.run(host="0.0.0.0", port=run_config.port, debug=(not run_config.prod))


if __name__ == "__main__":
    run(parse_args())
