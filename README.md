# Install

Install [poetry](https://github.com/sdispater/poetry)

```bash
$ git clone <repo-url>
$ cd achivka2
$ poetry install
```

# Run

```bash
$ poetry run python main.py
```

# Docker

Build image:

```bash
$ make build
```

Docker compose:

```bash
$ docker-compose up
```

You can scale:

```
$ docker-compose up --scale backend=2
```

The backend will be available at `http://<your-ip>.xip.io`.

# Load test

```bash
$ COUNT=2 MAX=100 URL=http://172.18.201.207.xip.io/incr ./load.sh
```

где COUNT - количество воркеров
MAX - максимальное число
