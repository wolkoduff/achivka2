FROM inn0kenty/pyinstaller-alpine:3.7 as builder

WORKDIR /build

COPY pyproject.toml .
COPY poetry.lock .
RUN poetry install --no-dev

COPY . .

ARG PYINSTALLER_ARG

RUN VPATH="/root/.cache/pypoetry/virtualenvs/*/lib/python*/site-packages/" \
    PYTHONPATH="$(find $VPATH -maxdepth 0 -print)" \
    /pyinstaller/pyinstaller.sh --skip-req-install --random-key $PYINSTALLER_ARG

FROM alpine:3.7

RUN addgroup -g 1000 -S app && adduser -S -u 1000 -G app app

LABEL maintainer="Innokenty Lebedev"

COPY --from=builder --chown=1000:1000 /build/dist/app .

USER app

EXPOSE 8000

ENTRYPOINT ["./app", "--prod"]
